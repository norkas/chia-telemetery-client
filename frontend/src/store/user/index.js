import packageSrc from '../../../../package.json';

const state = () => ({
    pathStatus: false,
    isAuth: false,
    newVersion: packageSrc.version,
    blockVersion: false,
    balance: 0,
    pendingWithdrawals: [],
    ID: '',
});

const getters = {
    isAuth: state => state.isAuth,
    isPathStatus: state => state.pathStatus,
    getID: state => state.ID,
    getBalance: state => state.balance,
};

const mutations = {
    setPathStatus: (state, status) => {
        state.pathStatus = status;
    },
    setAuth:(state, newVal) => {
        state.isAuth = newVal;
    },
    setNewVersion: (state, newVal) => {
        state.newVersion = newVal;
    },
    setBlockVersion: (state, status) => {
        state.blockVersion = status;
    },
    setBalance: (state, newVal) => {
        state.balance = newVal;
    },
    setPendingWithdrawals: (state, newVal) => {
        state.pendingWithdrawals = newVal;
    },
    setID(state, ID) {
        state.ID = ID;
    },
    logout: (state) => {
        state.isAuth = false;
        state.pathStatus = false;
    }
};

const actions = {};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}
