$(document).ready((e) => {
    $('a#plots-link').addClass('active-success');
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $("#addPlot").on('shown.bs.modal', (event) => {
        const ram = navigator.deviceMemory * 1024,
            delay = navigator.hardwareConcurrency,
            inputRam = $("input#ram"),
            helpRam = $("small#ramHelp"),
            inputThreads = $("input#threads"),
            helpThreads = $("small#helpThreads");

        inputRam.val(ram / 2);
        inputRam.attr('min', ram / 2);
        inputRam.attr('max', ram);
        helpRam.text(`минимум = ${ram / 2}, максимум = ${ram}`)
        inputThreads.val(delay / 2);
        inputThreads.attr('min', delay / 2);
        inputThreads.attr('max', delay)
        helpThreads.text(`минимум = ${delay / 2}, максимум = ${delay}`)
    })
    $('.btn#add-plot').click((event) => {
        $.ajax('/plots/add-plots', {
            method: 'post',
            dataType: 'JSON',
            data: {
                size: $("select#size").val(),
                quantityPlots: $("input#quantityPlots").val(),
                ram: $("input#ram").val(),
                threads: $("input#threads").val(),
                segments: $("input#segments").val(),
                disablesBitfield: $("input#disables-bitfield").val(),
                tempDirectory: $("input#temp-directory").val(),
                finalDirectory: $("input#final-directory").val()
            }
        })
            .then((data) => {
                console.log(data)
            })
            .catch(e => {
                console.log(e)
            })

    })
})