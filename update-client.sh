#!/bin/bash

#stop client
pm2 stop all

#stop chia
cd ~/chia-blockchain/
. ./activate
chia stop all -d
chia stop all
deactivate

#update client
cd ~/ecopool-client/
git reset --hard
git pull
yarn install

#restart client
pm2 restart all

echo "update completed!"
