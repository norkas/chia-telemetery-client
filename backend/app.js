const config = require('config');
const express = require('express');
const http = require('http');
const path = require('path');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const SocketIO = require('socket.io');
const isDevelop = require('./utils/is-develop');
const ChiaController = require('./controllers/chia-controller');
const getLogs = require('./utils/get-logs');
const { debugLog, debugError } = getLogs('app');
const app = express();

const normalizePort = (val) => {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
};
const onListening = () => {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;

    console.log(`Your server started on http://localhost:${config.port}`);
    debugLog(`Listening on ${bind}`);
};
const onError = (error) => {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    switch (error.code) {
        case 'EACCES':
            debugError(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            debugError(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
};
const port = normalizePort(config.port || 3401);

app.set('port', port);
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(morgan('combined'));
app.set('port', port);

if (isDevelop) {
    app.use(cors({
        origin(origin, setOrigin) {
            setOrigin(null, origin);
            return origin;
        },
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
        optionsSuccessStatus: 200
    }));
}

const server = http.createServer(app);

if (isDevelop) {
    app.io = SocketIO(server, {
        cors: {
            origin: '*',
            methods: ['GET', 'POST']
        }
    });
} else {
    app.io = SocketIO(server);
}

const HttpRoutes = require('./routes');
const ErrorsHandling = require('./routes/errors-handling');

app.use('/', express.static(path.resolve(process.cwd(), 'dist')));

HttpRoutes(app);
app.use(ErrorsHandling);
ChiaController.init();

server.listen(port, `0.0.0.0`);
server.on('error', onError);
server.on('listening', onListening);

module.exports = app;
