module.exports = {
    init: false,
    harvesterIsWorking: false,
    token: null,
    queues: {},
    histories: {
        checkPlots: [],
        farmingPlots: [],
        proofs: []
    },
    totalPlotsCount: 0
};
