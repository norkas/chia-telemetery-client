const crypto = require('crypto');

module.exports = (content, inputEncoding = 'utf8', outputEncoding='base64') => {
    const shaSum = crypto.createHash('sha256');
    shaSum.update(content, inputEncoding);
    return shaSum.digest(outputEncoding);
};
