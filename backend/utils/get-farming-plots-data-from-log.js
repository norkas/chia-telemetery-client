const stringRegex = /.+ [\d]{1,} plots were eligible for farming .+/gm;
const dateRegex = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3})/;
const eligiblePlotsDataRegex = /[\d]{1,} plots were eligible/gm;
const proofsDataRegex = /Found [\d]{1,} proofs/gm;
const timeDataRegex = /Time: [\d.]{1,} s/gm;
const totalPlotsDataRegex = /Total [\d]{1,} plots/gm;
const numberRegex = /[\d.]{1,}/gm;

module.exports = (logString) => {
    const match = logString.match(stringRegex);

    if (
        logString.indexOf('chia.harvester.harvester') === -1 ||
        !match
    ) {
        return false;
    }

    const stringWithData = match[0];
    const stringWithDateData = stringWithData.match(dateRegex)[0];
    const stringWithEligiblePlotsData = stringWithData.match(eligiblePlotsDataRegex)[0];
    const stringWithProofsData = stringWithData.match(proofsDataRegex)[0];
    const stringWithTimeData = stringWithData.match(timeDataRegex)[0];
    const stringWithTotalPlotsData = stringWithData.match(totalPlotsDataRegex)[0];

    return {
        date: stringWithDateData,
        count: stringWithEligiblePlotsData.match(numberRegex)[0],
        proofs: stringWithProofsData.match(numberRegex)[0],
        time: stringWithTimeData.match(numberRegex)[0],
        totalPlots: stringWithTotalPlotsData.match(numberRegex)[0]
    };
};
