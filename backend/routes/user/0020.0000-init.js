const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');

router.get('/api/user/init', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    return res.send({
        success: true,
        answer: {
            id: GuiController.localConfig.get('userId')
        }
    });
});

module.exports = { router };
