const router = require('express').Router();
const GuiController = require('../../controllers/gui-controller');
const ChiaController = require('../../controllers/chia-controller');

router.get('/api/check-login', async (req, res) => {
    const answer = {
        success: GuiController.userIsLogged(),
        answer: {
            checkChia: await ChiaController.checkCLI()
        }
    };

    return res.send(answer);
});

module.exports = { router };
