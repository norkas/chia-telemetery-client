const router = require('express').Router();
const EcoPoolController = require('../../controllers/ecopool-controller');
const ChiaController = require('../../controllers/chia-controller');
// const GuiController = require('../../controllers/gui-controller');

router.post('/api/login', async (req, res) => {
    const { body: { login, password } } = req;
    const requestData = await EcoPoolController.userLogin({
        login,
        password
    });

    if (!requestData.success) {
        return res.send(requestData);
    }

    const checkChia = await ChiaController.checkCLI();
    const answer = {
        success: true,
        answer: { checkChia }
    };

    if (checkChia) {
        ChiaController.init();
        // GuiController.setupConnection();
        return res.send(answer);
    } else {
        return res.send(answer);
    }
});

module.exports = { router };
