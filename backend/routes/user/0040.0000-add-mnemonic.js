const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.post('/api/user/add-mnemonic', async (req, res, next) => {
    const { body: { mnemonic } } = req;

    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    if (!mnemonic) {
        return { success: false };
    }

    const requestData = await EcoPoolController.addMnemonic(mnemonic);
    const { success } = requestData;

    return res.send({ success });
});

module.exports = { router };
