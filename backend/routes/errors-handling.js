const ROUTES_ERROR_ANSWERS = require('../constants/routes-error-answers');
const isDevelop = require('../utils/is-develop');
const getLogs = require('../utils/get-logs');
const { debugError } = getLogs('routes:errors-handling');

module.exports = (error, req, res) => {
    debugError(JSON.stringify({
        message: 'Route error',
        error
    }));

    res.locals.message = error.message;
    res.locals.error = isDevelop ? error : {};

    const status = error.status || 500;

    res.status(status);
    res.send(ROUTES_ERROR_ANSWERS[status]);
};
