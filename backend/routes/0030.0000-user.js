const fs = require('fs');
const routerModules = fs.readdirSync(`${__dirname}/user/`)
    .sort((a, b) => {
        if (a < b) {
            return -1;
        }

        if (a > b) {
            return 1;
        }

        return 0;
    })
    .map(fileName => {
        return require(`./user/${fileName}`);
    });

module.exports = {
    routes: routerModules
};
