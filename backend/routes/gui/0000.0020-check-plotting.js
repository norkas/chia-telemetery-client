const router = require('express').Router();
const state = require('../../state');

router.get('/api/gui/check-plotting', (req, res) => {
    const { queues } = state;
    const queuesNames = Object.keys(queues);
    const hasQueues = Boolean(queuesNames);

    if (!hasQueues) {
        return res.send({ success: false });
    }

    let plotsInQueues = false;

    for (const queueName of queuesNames) {
        if (queues[queueName] && queues[queueName].length) {
            plotsInQueues = true;
        }
    }

    return res.send({ success: plotsInQueues });
});

module.exports = { router };
