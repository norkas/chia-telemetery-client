const router = require('express').Router();
const GuiController = require('../../controllers/gui-controller');
const state = require('../../state');

router.get('/api/gui/stop-plotting', async (req, res) => {
    const { queues } = state;
    const queuesNames = Object.keys(queues);
    const hasQueues = Boolean(queuesNames.length);

    if (!hasQueues) {
        return res.send({ success: true });
    }

    let checkAllRemove = true;

    for (const queueName of queuesNames) {
        const plotsIds = queues[queueName].map(plotData => {
            return plotData.id;
        });

        for (const plotId of plotsIds) {
            const removeStatus = GuiController.removePlot({
                queue: queueName,
                plotId,
                onlyRemove: true
            });

            if (!removeStatus) {
                checkAllRemove = removeStatus;
            }
        }
    }

    return res.send({ success: checkAllRemove });
});

module.exports = { router };
