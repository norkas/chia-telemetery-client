const semver = require('semver');
const router = require('express').Router();
const axios = require('axios');
const isDevelop = require('../../utils/is-develop');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('routes:gui:get-version');
const packageJson = require('../../../package.json');

const isBlockVersion = (newVersion, blockVersions) => {
    return blockVersions.some(blockVersion => {
        return semver.gte(newVersion, blockVersion) && semver.lt(packageJson.version, blockVersion);
    });
};

router.get('/api/gui/get-version', async (req, res) => {
    let requestData;

    try {
        requestData = await axios({
            method: 'get',
            url: `https://gitlab.com/ecopool-public/chia-telemetery-client/-/raw/${isDevelop ? 'development' : 'master'}/package.json`
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: 'An error occured when requesting the gui version from the repository',
            error
        }));
        return { success: false };
    }

    debugLog('Gui version requested successfully');

    const {
        data: {
            version,
            blockVersions
        }
    } = requestData;

    res.send({
        success: true,
        answer: {
            version,
            blockVersion: !blockVersions ? false : isBlockVersion(version, blockVersions)
        }
    });
});

module.exports = { router };
