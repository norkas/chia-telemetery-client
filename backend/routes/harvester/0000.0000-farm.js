const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const ChiaController = require('../../controllers/chia-controller');

router.get('/api/harvester/farm', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const plots = await ChiaController.getPlots(true);
    const badPlots = await ChiaController.getBadPlots();

    return res.send({
        success: true,
        answer: { plots, badPlots }
    });
});

module.exports = { router };
