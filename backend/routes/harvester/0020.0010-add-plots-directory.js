const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const ChiaController = require('../../controllers/chia-controller');

router.post('/api/harvester/add-plots-directory', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const { body: { path } } = req;
    const addStatus = await ChiaController.addPlotsDirectory(path);

    return res.send({ success: addStatus });
});

module.exports = { router };
