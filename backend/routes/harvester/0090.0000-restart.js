const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const ChiaController = require('../../controllers/chia-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('routes:harvester:restart');

router.get('/api/harvester/restart', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    debugLog('Harvester restart started');

    const restartHarvesterStatus = await ChiaController.startHarvester();

    if (!restartHarvesterStatus) {
        debugError('An error occurred while restarting the harvester');

        return res.send({
            success: false
        });
    }

    debugLog('Harvester restarted');

    return res.send({
        success: true
    });
});

module.exports = { router };
