const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const state = require('../../state');

router.get('/api/harvester/get-creating-plots', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const queues = state.queues;
    const queuesNames = Object.keys(queues);
    const preparedQueues = queuesNames.reduce((acc, queueName) => {
        acc[queueName] = queues[queueName].map((plotData) => {
            const { id, starting, options } = plotData;
            return { id, starting, options };
        });

        return acc;
    }, {});

    return res.send({
        success: true,
        answer: { queues: preparedQueues }
    });
});

module.exports = { router };
