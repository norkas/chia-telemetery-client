const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const ChiaController = require('../../controllers/chia-controller');

router.post('/api/harvester/remove-plots-directory', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const { body: { path } } = req;
    const removeStatus = await ChiaController.removePlotsDirectory(path);

    return res.send({ success: removeStatus });
});

module.exports = { router };
