const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
let socket = null;

router.post('/api/harvester/create-plots', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {
        body: {
            plotParams,
            madPlotter = false
        }
    } = req;
    const createPlotsStatus = GuiController.createPlots(plotParams, socket, madPlotter);

    if (!createPlotsStatus) {
        return res.send({ success: false });
    }

    return res.send({ success: true });
});

module.exports = {
    needSocket: true,
    setSocket: (socketInstance) => {
        socket = socketInstance;
    },
    router
};
