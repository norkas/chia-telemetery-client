const localConfig = require('./local-config');
const state = require('../../state');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('gui-controller:stop');

module.exports = () => {
    state.token = null;
    localConfig.remove('currentUser');
    localConfig.remove('userId');
    debugLog('The running gui controller processes have been successfully stopped');
};
