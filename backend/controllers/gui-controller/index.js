const localConfig = require('./local-config');
const setupConnection = require('./setup-connection');
const getCorrectConnection = require('./get-correct-connection');
const userIsLogged = require('./user-is-logged');
const createPlot = require('./create-plot');
const createPlots = require('./create-plots');
const removePlot = require('./remove-plot');
const stop = require('./stop');

module.exports = {
    localConfig,
    setupConnection,
    getCorrectConnection,
    userIsLogged,
    createPlot,
    createPlots,
    removePlot,
    stop
};
