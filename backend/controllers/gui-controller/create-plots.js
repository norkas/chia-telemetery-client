const md5 = require('md5');
const createPlot = require('./create-plot');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('gui-controller:create-plots');
const state = require('../../state');

module.exports = (payload = {}, socket, madPlotter) => {
    const {
        size,
        ram,
        threads,
        count,
        buckets,
        bitField,
        queue = 'default',
        tempFolder,
        tempFolder2,
        persistentFolder,
        plottingType,
        plottingDelay
    } = payload;

    if (!count) {
        debugError('Number of plots not transmitted');
        return false;
    }

    const plotsData = [];
    const { queues } = state;
    const preparedPlottingDelay = plottingDelay * 60 * 1000;

    for (let i = 0; i < count; i++) {
        plotsData.push({
            id: md5(Date.now() + Math.random()),
            starting: false,
            options: {
                size,
                ram,
                threads,
                buckets,
                bitField,
                tempFolder,
                tempFolder2,
                persistentFolder
            },
            logs: [],
            process: null,
            parallel: {
                active: plottingType === 1,
                delay: preparedPlottingDelay * i
            },
            madPlotter
        });
    }

    if (!plotsData.length) {
        debugError('Empty array with plot data');
        return false;
    }

    if (queues[queue] && queues[queue].length) {
        queues[queue] = [
            ...queues[queue],
            ...plotsData
        ];

        debugLog(`New plots have been successfully added to the ${queue} queue`);
    } else {
        queues[queue] = plotsData;
        debugLog(`Queue ${queue} successfully created with new plots`);
    }

    const createPlotStatus = createPlot(queue, socket);

    if (createPlotStatus === false) {
        return false;
    }

    return true;
};
