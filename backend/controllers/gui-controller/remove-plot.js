const kill = require('tree-kill');
const getCreatingPlotByIdAndQueueName = require('../../utils/get-creating-plot-by-id-and-queue-name');
const createPlot = require('./create-plot');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('gui-controller:remove-plot');
const state = require('../../state');

module.exports = (payload) => {
    const {
        queue,
        plotId,
        socket,
        onlyRemove = false
    } = payload;

    if (!queue) {
        debugError('Queue name not passed');
        return false;
    }

    if (!plotId) {
        debugError('Id of the plot not transmitted');
        return false;
    }

    const removePlotData = getCreatingPlotByIdAndQueueName(queue, plotId);

    if (!removePlotData) {
        debugError(`The plot was not found in the queue. (plotId: ${plotId}, queue: ${queue})`);
        return false;
    }

    const queueData = state.queues[queue];
    const removePlotIndex = queueData.findIndex((plotData) => {
        return plotData.id === plotId;
    });

    if (removePlotData.starting) {
        kill(queueData[removePlotIndex].process.pid, 'SIGKILL', error => {
            if (error) {
                debugError(JSON.stringify({
                    message: 'Process stop error',
                    error
                }));
            } else {
                debugLog(`The process of creating a plot with id: ${plotId} has been stopped`);
            }
        });
    }

    queueData.splice(removePlotIndex, 1);

    if (queueData.length) {
        if (!onlyRemove) {
            createPlot(queue, socket);
            debugLog(`Creation of next plot in queue ${queue} started`);
        }
    } else {
        delete state.queues[queue];
        debugLog(`Queue ${queue} has been deleted`);
    }

    debugLog(`Plot with id: ${plotId} removed`);
    return true;
};
