const localConfig = require('./local-config');
const EcoPoolController = require('../ecopool-controller');
const ChiaController = require('../chia-controller');
const checkConnectionSettings = require('../../utils/check-connection-settings');
const compareConnections = require('../../utils/compare-connections');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('gui-controller:get-connection');
const state = require('../../state');

module.exports = async (onlyGetConnectionData = false) => {
    const user = localConfig.get('currentUser');
    const userId = localConfig.get('userId');

    if (!user || !userId) {
        debugWarn('Haven`t user or user id or both');
        return false;
    }

    const requestData = await EcoPoolController.syncSocketRequest('connection-setup');

    if (!requestData) {
        debugError('Connection setup request doesn`t return data or return error');
        return false;
    }

    const { farmerIp, farmerPort } = requestData;
    const newConnectionData = {
        ip: farmerIp,
        port: farmerPort
    };
    const checkSettings = checkConnectionSettings({
        ...newConnectionData,
        debugError
    });

    if (!checkSettings) {
        return false;
    }

    if (onlyGetConnectionData) {
        return newConnectionData;
    }

    if (state.init) {
        const saveConnectionData = localConfig.get('connection');
        const matchConnections = compareConnections(saveConnectionData, newConnectionData);

        if (matchConnections) {
            debugWarn('New connection data and saved connection data matched');
            return false;
        }
    }

    const setFarmerPeerStatus = await ChiaController.setFarmerPeer(newConnectionData);

    if (!setFarmerPeerStatus) {
        return false;
    }

    debugLog('Connection data successfully setup');
    localConfig.set('connection', newConnectionData);
    ChiaController.restartHarvester();
};
