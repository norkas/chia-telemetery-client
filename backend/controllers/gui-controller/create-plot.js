const checkParallelPlotting = require('../../utils/check-parallel-plotting');
const checkPlottingInQueue = require('../../utils/check-plotting-in-queue');
const createDefaultPlot = require('../chia-controller/create-plot');
const createMadPlot = require('../mad-max-plotter-controller/create-plot');
const createTestPlot = require('../chia-controller/create-test-plot');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('gui-controller:create-plot');
const state = require('../../state');

const createPlot = async (queue, socket, force = false) => {
    if (!queue) {
        debugError('Queue name not passed');
        return false;
    }

    const queueData = state.queues[queue];

    if (!queueData) {
        debugError(`Queue data ${queue} not found`);
        return false;
    }

    const isParallelPlotting = checkParallelPlotting(queueData);
    const queueLength = queueData.length;

    if (isParallelPlotting) {
        for (const plotData of queueData) {
            if (!plotData.starting) {
                setTimeout(() => {
                    startPlotting({
                        queueData,
                        socket,
                        queue,
                        plotData
                    });
                    socket.emit('plot-complete');
                }, plotData.parallel.delay);
            }
        }
    } else {
        const queueIsPlotting = checkPlottingInQueue(queueData);

        if (queueIsPlotting && !force) {
            debugWarn(`Plotting in queue ${queue} is already running`);
            return;
        }

        const plotData = force ? queueData[queueLength - 2] : queueData[queueLength - 1];

        startPlotting({
            queueData,
            socket,
            queue,
            plotData
        });
    }
};
const sendLogStringAndSaveInHistory = (string, plotData, socket) => {
    const logData = {
        timestamp: Date.now(),
        string
    };

    plotData.logs.push(logData);
    socket.emit(`plot-${plotData.id}`, logData);
};

const startPlotting = (payload) => {
    const {
        queueData,
        socket,
        plotData,
        queue
    } = payload;
    const isParallel = plotData.parallel.active;

    plotData.starting = true;

    if (process.env.NODE_ENV === 'development') {
        plotData.process = createTestPlot(plotData.options.tempFolder, plotData.options.persistentFolder);
    } else {
        if (plotData.madPlotter) {
            plotData.process = createMadPlot(plotData.options);
        } else {
            plotData.process = createDefaultPlot(plotData.options);
        }
    }

    const plotProcess = plotData.process;

    debugLog(`Plotting in queue ${queue} started successfully`);

    plotProcess.stdout.on('data', (data) => {
        const logString = data.toString().trim();
        const isFinishCompilePlot = logString.includes('Time for phase 4') || logString.includes('Total plot creation time was');

        if (isFinishCompilePlot) {
            if (!isParallel) {
                createPlot(queue, socket, true);
            }
        }

        sendLogStringAndSaveInHistory(logString, plotData, socket);
    });

    plotProcess.stderr.on('data', (data) => {
        sendLogStringAndSaveInHistory(data.toString().trim(), plotData, socket);
    });

    plotProcess.on('exit', (exitCode) => {
        if (exitCode === 0) {
            if (!isParallel) {
                queueData.pop();
            } else {
                const plotIndex = queueData.findIndex($plotData => {
                    return $plotData.id === plotData.id;
                });
                queueData.splice(plotIndex, 1);
            }

            debugLog(`Plotting in queue ${queue} completed successfully`);

            if (!queueData.length) {
                delete state.queues[queue];
                debugLog(`Queue ${queue} completed successfully`);
            }
        } else if (exitCode !== null) {
            plotData.logs.push({
                timestamp: Date.now(),
                string: `Error code ${exitCode}`
            });
            debugError(`Plotting on queue ${queue} failed`);
        }

        socket.emit('plot-complete');
    });
};

module.exports = createPlot;
