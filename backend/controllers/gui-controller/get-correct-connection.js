const localConfig = require('./local-config');
const setupConnection = require('./setup-connection');
const compareConnections = require('../../utils/compare-connections');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn } = getLogs('gui-controller:get-correct-connection');

module.exports = async () => {
    const saveConnectionData = localConfig.get('connection');

    if (saveConnectionData) {
        const newConnectionData = await setupConnection(true);

        if (newConnectionData) {
            debugLog('New connection data successfully got');

            const matchConnections = compareConnections(saveConnectionData, newConnectionData);

            if (matchConnections) {
                debugLog('New connection data match with saved connection data, apply saved connection data');
                return saveConnectionData;
            } else {
                debugLog('New connection data don`t match with saved connection data, apply new connection data');
                return newConnectionData;
            }
        } else {
            debugWarn('Connection data could not be retrieved, start with saved connection data');
            return saveConnectionData;
        }
    } else {
        debugLog('User has not a saved connection data');

        const user = localConfig.get('currentUser');
        const userId = localConfig.get('userId');

        if (user && userId) {
            debugLog('User logged, try get setup connection');

            const connectionData = await setupConnection(true);

            if (connectionData) {
                debugLog('Connection data successfully got');
                return connectionData;
            } else {
                debugWarn('Connection data could not be retrieved, start with default connection data');
                return null;
            }
        } else {
            debugWarn('User not logged, start with default connection data');
            return null;
        }
    }
};
