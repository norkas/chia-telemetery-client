const localConfig = require('../gui-controller/local-config');
const socketIoClient = require('./socket-io-client');
const socketIoClientForSendPlotsOnServer = require('./socket-io-client-for-send-plots-on-server');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:set-user');
const setUserForMainSocket = (login) => {
    socketIoClient.emit('setupUser', login, (payload) => {
        const { success, id } = payload;

        if (!success) {
            debugError('Failed to login to the server (main socket)');
            return false;
        }

        localConfig.set('userId', id);
        debugLog(`User with id: ${id}, successfully logged in to the server (main socket)`);
    });
};
const setUserForPlotsSocket = (login) => {
    socketIoClientForSendPlotsOnServer.emit('setupUser', login, (payload) => {
        const { success, id } = payload;

        if (!success) {
            debugError('Failed to login to the server (plots socket)');
            return false;
        }

        localConfig.set('userId', id);
        debugLog(`User with id: ${id}, successfully authorized on the server (plots socket)`);
    });
};

module.exports = {
    mainSocket: setUserForMainSocket,
    plotsSocket: setUserForPlotsSocket,
    allSockets: (login) => {
        localConfig.set('currentUser', login);
        setUserForMainSocket(login);
        setUserForPlotsSocket(login);
    }
};
