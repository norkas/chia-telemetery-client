const apiRequest = require('./api-request');
const socketIoClient = require('./socket-io-client');
const socketIoClientForSendPlotsOnServer = require('./socket-io-client-for-send-plots-on-server');
const syncSocketRequest = require('./sync-socket-request');
const setUser = require('./set-user');
const userRegistration = require('./registration');
const userLogin = require('./login');
const userLogout = require('./logout');
const addPlots = require('./add-plots');
const plotsUploader = require('./plots-uploader');
const poolVolume = require('./pool-volume');
const userBalance = require('./user-balance');
const userVolume = require('./user-volume');
const getUserTransactionsHistory = require('./get-user-transactions-history');
const makeWithdraw = require('./make-withdraw');
const pendingWithdrawals = require('./pending-withdrawals');
const addMnemonic = require('./add-mnemonic');
const getMnemonics = require('./get-mnemonics');
const stop = require('./stop');

module.exports = {
    addMnemonic,
    getMnemonics,
    apiRequest,
    socketIoClient,
    socketIoClientForSendPlotsOnServer,
    syncSocketRequest,
    setUser,
    userRegistration,
    userLogin,
    userLogout,
    getUserTransactionsHistory,
    addPlots,
    plotsUploader,
    poolVolume,
    userVolume,
    userBalance,
    makeWithdraw,
    pendingWithdrawals,
    stop
};
