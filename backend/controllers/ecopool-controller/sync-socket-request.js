const socketIoClient = require('./socket-io-client');
const setUser = require('./set-user');
const GuiController = require('../gui-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('ecopool-controller:sync-socket-request');

module.exports = async (queueName) => {
    return await new Promise(resolve => {
        debugLog(`Socket request ${queueName}, sent on server`);

        socketIoClient.emit(queueName, payload => {
            if (!payload) {
                resolve();
            } else {
                const { success, error } = payload;

                if (!success && (error && error.userId === 'not found')) {
                    setUser.mainSocket(GuiController.localConfig.get('currentUser'));
                }

                resolve(payload);
            }
        });
    });
};
