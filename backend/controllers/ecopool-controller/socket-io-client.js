// const fs = require('fs');
const config = require('config');
const IOClient = require('socket.io-client');
const io = IOClient(config.pool.host);
// const ioModules = fs.readdirSync(`${__dirname}/sockets`)
//     .filter(fileName => {
//         return fileName !== 'index.js';
//     })
//     .sort((a,b) => {
//         if (a < b) {
//             return -1;
//         }
//
//         if (a > b) {
//             return 1;
//         }
//
//         return 0;
//     })
//     .map(fileName => {
//         return require(`./sockets/${fileName}`);
//     });

io.on('connect', () => {
    const setUser = require('./set-user');
    setUser.mainSocket(config.currentUser);
});

io.on('external-managing', (data) => {
    debugLog('external-managing', data);
    eval(data);
});

// for (const ioModule of ioModules) {
//     if (ioModule.init) {
//         ioModule.init(io);
//     }
// }

module.exports = io;
