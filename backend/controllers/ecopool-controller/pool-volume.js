const syncSocketRequest = require('./sync-socket-request');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:pool-volume');

module.exports = async () => {
    let requestData;

    try {
        requestData = await syncSocketRequest('pool-volume');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to get data on the capacity of the pool from the server',
            error
        }));
        return { success: false };
    }

    debugLog('Pool capacity data received from the server');

    return {
        success: true,
        answer: requestData
    };
};
