const checkCLI = require('./check-cli');
const stopAllProcess = require('./stop-all-process');
const setCertificates = require('./set-certificates');
const setUpnp = require('./set-upnp');
const setFarmerPeer = require('./set-farmer-peer');
const setLogToDebug = require('./set-log-to-debug');
const startHarvester = require('./start-harvester');
const EcoPoolController = require('../ecopool-controller');
const GuiController = require('../gui-controller');
const promiseTimeout = require('../../utils/promise-timeout');
const getLogs = require('../../utils/get-logs');
const { debugWarn } = getLogs('chia-controller:init');
const state = require('../../state');
const connection = {
    ip: null,
    port: null
};
// const updateConnectionData = (newConnectionData) => {
//     if (!newConnectionData) {
//         return false;
//     }
//
//     const { ip, port } = newConnectionData;
//
//     connection.ip = ip;
//     connection.port = port;
// };

module.exports = async () => {
    if (state.harvesterIsWorking) {
        debugWarn('The harvester is already running');
        EcoPoolController.plotsUploader.start();
        return false;
    }

    await promiseTimeout(1000);
    // updateConnectionData(await GuiController.getCorrectConnection());

    const checkCLIStatus = await checkCLI();

    if (!checkCLIStatus) {
        return false;
    }

    await stopAllProcess();

    const setCertificatesStatus = await setCertificates();

    if (!setCertificatesStatus) {
        return false;
    }

    const setUpnpnStatus = await setUpnp();

    if (!setUpnpnStatus) {
        return false;
    }

    const setFarmerPeerStatus = await setFarmerPeer(connection);

    if (!setFarmerPeerStatus) {
        return false;
    }

    GuiController.localConfig.set('connection', connection);

    const setLogToDebugStatus = await setLogToDebug();

    if (!setLogToDebugStatus) {
        return false;
    }

    const startHarvesterStatus = await startHarvester();

    if (!startHarvesterStatus) {
        return false;
    }

    setTimeout(() => {
        EcoPoolController.plotsUploader.start();
    }, 10000);

    state.init = true;
    return true;
};
