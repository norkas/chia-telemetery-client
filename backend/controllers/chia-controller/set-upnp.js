const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:set-upnp');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            configure: null,
            '--enable-upnp': null,
            false: null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog('Successfully set upnp = false');
    } else {
        debugError('Failed to set upnp = false');
    }

    return checkCommand.status;
};
