const callCLI = require('./call-cli');

module.exports = (payload) => {
    return new Promise(resolve => {
        const child = callCLI(payload);

        child.on('exit', function (exitCode) {
            resolve({
                exitCode,
                status: exitCode === 0
            });
        });
    });
};
