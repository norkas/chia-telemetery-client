const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:start-harvester');
const state = require('../../state');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            start: null,
            harvester: null,
            '-r': null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        state.harvesterIsWorking = true;
        debugLog('Harvester successfully launched');
    } else {
        debugError('Failed to start harvester');
    }

    return checkCommand.status;
};
