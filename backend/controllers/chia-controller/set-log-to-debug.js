const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:set-log-to-debug');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            configure: null,
            '--set-log-level': null,
            DEBUG: null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog('Chia logs installed successfully in DEBUG mode');
    } else {
        debugError('Failed to install chia logs in DEBUG mode');
    }

    return checkCommand.status;
};
