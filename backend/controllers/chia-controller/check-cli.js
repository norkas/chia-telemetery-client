const discoverCLI = require('./discover-cli');
const callCLIPromise = require('./call-cli-promise');
const GuiController = require('../gui-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:check-cli');

module.exports = async (customPath = null) => {
    const CLIPath = customPath ? customPath : discoverCLI();
    const checkCLICommand = await callCLIPromise({
        commandPayload: {
            '-h': null
        },
        customCLIPath: CLIPath,
        commandForStartHarvester: true
    });

    if (checkCLICommand.status) {
        GuiController.localConfig.set('chiaCliPath', CLIPath);
        debugLog('Path to chia-blockchain found successfully');
    } else {
        debugError('Path to chia-blockchain not found');
    }

    return checkCLICommand.status;
};
