const HarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:get-bad-plots');
const state = require('../../state');

module.exports = async () => {
    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    let plotsData;

    try {
        plotsData = await HarvesterInstance.getPlots();
    } catch (error) {
        console.error(error);
        debugError('getPlots method by HarvesterInstance failed');
        return [];
    }

    if (!plotsData) {
        debugError('getPlots method by HarvesterInstance return undefined');
        return [];
    }

    if (!plotsData.success) {
        debugError('getPlots method by HarvesterInstance success = false');
        return [];
    }

    const badPlots = plotsData.not_found_filenames;

    if (!badPlots.length) {
        debugLog('User has no invalid plots');
        return [];
    }

    debugWarn('Data on invalid plots successfully collected');

    return badPlots;
};
