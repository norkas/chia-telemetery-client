const path = require('path');
const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:set-certificates');

module.exports = async () => {
    const certificatesPath = path.join(process.cwd(), '/pool-certificates');
    const checkCommand = await callCLIPromise({
        commandPayload: {
            init: null,
            '-c': null,
            [certificatesPath]: null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog('Pool certificates installed successfully');
    } else {
        debugError('Failed to install pool certificates');
    }

    return checkCommand.status;
};
