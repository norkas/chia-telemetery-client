const config = require('config');
const callCLI = require('./call-cli');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:create-test-plot');
const poolKeys = config.poolKeys;

module.exports = (tempFolder, persistentFolder) => {
    if (!tempFolder || !persistentFolder) {
        debugError('Temporary folder or persistent folder not transferred');
        return false;
    }

    debugLog('Test plot creation started');

    return callCLI({
        commandPayload: {
            plots: null,
            create: null,
            '-k': 25,
            '--override-k': null,
            '-r': 8, // количество потоков
            '-b': 4096, // RAM
            '-n': 1, // количество участков подряд
            '-u': 64, // количество корзин
            '-t': tempFolder, // временная директория
            '-d': persistentFolder, // финальная директория
            '-f': poolKeys.farmer,
            '-p': poolKeys.pool
        },
        commandForStartHarvester: true
    });
};
