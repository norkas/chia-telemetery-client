const HarvesterInstance = require('./harvester-instance');
const discoverCLI = require('./discover-cli');
const callCLI = require('./call-cli');
const callCLIPromise = require('./call-cli-promise');
const checkCLI = require('./check-cli');
const stopAllProcess = require('./stop-all-process');
const setCertificates = require('./set-certificates');
const setUpnp = require('./set-upnp');
const setFarmerPeer = require('./set-farmer-peer');
const setLogToDebug = require('./set-log-to-debug');
const startHarvester = require('./start-harvester');
const restartHarvester = require('./restart-harvester');
const getHarvesterId = require('./get-harvester-id');
const logWatcher = require('./log-watcher');
const createTestPlot = require('./create-test-plot');
const createPlot = require('./create-plot');
const getPlots = require('./get-plots');
const getBadPlots = require('./get-bad-plots');
const addPlotsDirectory = require('./add-plots-directory');
const removePlotsDirectory = require('./remove-plots-directory');
const getPlotsDirectories = require('./get-plots-directories');
const init = require('./init');

module.exports = {
    HarvesterInstance,
    discoverCLI,
    callCLI,
    callCLIPromise,
    checkCLI,
    stopAllProcess,
    startHarvester,
    restartHarvester,
    logWatcher,
    setCertificates,
    setUpnp,
    setFarmerPeer,
    setLogToDebug,
    getHarvesterId,
    createTestPlot,
    createPlot,
    getPlots,
    getBadPlots,
    addPlotsDirectory,
    removePlotsDirectory,
    getPlotsDirectories,
    init
};
