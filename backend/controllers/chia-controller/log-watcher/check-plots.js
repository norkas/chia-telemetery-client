const getCheckPlotsDataFromLog = require('../../../utils/get-check-plots-data-from-log');
const getLogs = require('../../../utils/get-logs');
const { debugLog } = getLogs('chia-controller:log-watcher:check-plots');
const state = require('../../../state');

let lastTime = null;

module.exports = (lastLogString, sockets) => {
    const plotsData = getCheckPlotsDataFromLog(lastLogString);

    if (!plotsData) {
        return false;
    }

    const time = new Date().getTime();

    if (
        time === lastTime ||
        time - lastTime < 1000
    ) {
        return false;
    }

    const checkPlotsItem = {
        time: plotsData.time,
        timestamp: time
    };

    state.histories.checkPlots.unshift(checkPlotsItem);
    state.histories.checkPlots = state.histories.checkPlots.slice(0, 200);
    lastTime = time;

    debugLog('Data about plots checking sent to client');

    for (const socket of sockets) {
        socket.emit('check-plots', checkPlotsItem);
    }
};
