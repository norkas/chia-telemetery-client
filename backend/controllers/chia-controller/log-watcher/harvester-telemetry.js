const EcoPoolController = require('../../ecopool-controller');
const getCheckPlotsDataFromLog = require('../../../utils/get-check-plots-data-from-log');
const getFarmingPlotsDataFromLog = require('../../../utils/get-farming-plots-data-from-log');
const getHarvesterConnectionTimeFromLog = require('../../../utils/get-harvester-connection-time-from-log');
const getBytesFromTibs = require('../../../utils/get-bytes-from-tibs');
const getHarvesterId = require('../get-harvester-id');
const getLogs = require('../../../utils/get-logs');
const { debugLog } = getLogs('chia-controller:log-watcher:harvester-telemetry');
const state = require('../../../state');

const telemetryData = {
    totalPlots: null,
    totalPlotsSize: null,
    eligiblePlots: null,
    proofs: null,
    checkPlotsTime: null,
    eligiblePlotsTime: null,
    challengeResponseTime: null
};

let lastFarmingTime = null;
let lastChallengeResponseTime = null;

module.exports = (lastLogString, sockets) => {
    const totalPlotsData = getCheckPlotsDataFromLog(lastLogString);
    const farmingPlotsData = getFarmingPlotsDataFromLog(lastLogString);
    const connectionTime = getHarvesterConnectionTimeFromLog(lastLogString);

    if (totalPlotsData) {
        telemetryData.totalPlots = totalPlotsData.count;
        telemetryData.totalPlotsSize = getBytesFromTibs(totalPlotsData.size);
        telemetryData.checkPlotsTime = totalPlotsData.time;
    }

    if (farmingPlotsData) {
        telemetryData.eligiblePlots = farmingPlotsData.count;
        telemetryData.proofs = farmingPlotsData.proofs;
        telemetryData.eligiblePlotsTime = farmingPlotsData.time;

        const farmingTime = new Date(farmingPlotsData.date).getTime();

        if (lastFarmingTime !== farmingTime) {
            const filterItem = {
                timestamp: farmingTime,
                passedFilter: farmingPlotsData.count,
                proofs: farmingPlotsData.proofs,
                total: farmingPlotsData.totalPlots
            };
            const proofsItem = {
                proofs: farmingPlotsData.proofs,
                timestamp: new Date().getTime()
            };
            state.histories.farmingPlots.unshift(filterItem);
            state.histories.farmingPlots = state.histories.farmingPlots.slice(0, 10);
            state.histories.proofs.unshift(proofsItem);
            state.histories.proofs = state.histories.proofs.slice(0, 50);

            debugLog('Data about eligible plots and proofs sent to client');

            for (const socket of sockets) {
                socket.emit('harvester-telemetry', {
                    filter: filterItem,
                    proofs: proofsItem
                });
            }
        }

        lastFarmingTime = farmingTime;
    }

    let $challengeResponseTime;

    if (connectionTime) {
        const utcConnectionTime = new Date(connectionTime).toISOString();

        telemetryData.challengeResponseTime = new Date(utcConnectionTime).getTime();
        $challengeResponseTime = new Date(connectionTime).getTime();
    }

    const {
        totalPlots,
        totalPlotsSize,
        eligiblePlots,
        proofs,
        checkPlotsTime,
        eligiblePlotsTime,
        challengeResponseTime
    } = telemetryData;

    if (
        totalPlots &&
        totalPlotsSize &&
        eligiblePlots &&
        proofs &&
        checkPlotsTime &&
        eligiblePlotsTime &&
        challengeResponseTime &&
        (lastChallengeResponseTime !== $challengeResponseTime && $challengeResponseTime)
    ) {
        const harvesterId = getHarvesterId();

        telemetryData.harvesterId = harvesterId;
        EcoPoolController.socketIoClientForSendPlotsOnServer.emit('harvester-telemetry', telemetryData);
        debugLog(`Harvester id: ${harvesterId}. Telemetry data transferred to server`);
        telemetryData.eligiblePlots = null;
        telemetryData.proofs = null;
        telemetryData.eligiblePlotsTime = null;
        telemetryData.challengeResponseTime = null;
        lastChallengeResponseTime = $challengeResponseTime;
    }
};
