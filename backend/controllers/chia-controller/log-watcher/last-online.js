const getHarvesterConnectionTimeFromLog = require('../../../utils/get-harvester-connection-time-from-log');
const getLogs = require('../../../utils/get-logs');
const { debugLog } = getLogs('chia-controller:log-watcher:last-online');

let oldDateOnline = null;

module.exports = (lastLogString, sockets) => {
    const date = getHarvesterConnectionTimeFromLog(lastLogString);

    if (!date) {
        return false;
    }

    const lastDateOnline = new Date(date).getTime();
    let timeDiff = 0;

    if (oldDateOnline) {
        timeDiff = (lastDateOnline - oldDateOnline) / 1000;
    }

    oldDateOnline = lastDateOnline;

    if (timeDiff < 1) {
        return false;
    }

    const connection = timeDiff < 20
        ? 'perfect'
        : timeDiff >= 20 && timeDiff < 60
            ? 'good'
            : 'bad'
    ;
    const connectionData = { connection };

    debugLog(`Data about connection harvester and farmer sent to client. (Connection: ${connection})`);

    for (const socket of sockets) {
        socket.emit('harvester-connection', connectionData);
    }
};
