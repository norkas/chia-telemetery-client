const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn } = getLogs('chia-controller:stop-all-process');
const state = require('../../state');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            stop: null,
            all: null,
            '-d': null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        state.harvesterIsWorking = false;
        debugLog('All chia processes stopped successfully');
    } else {
        debugWarn('Failed to stop all chia processes');
    }

    return checkCommand.status;
};
