const config = require('config');
const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:set-farmer-peer');

module.exports = async (payload) => {
    const { ip, port } = payload;
    const $ip = ip || config.pool.ip;
    const $port = port || '8447';

    const checkCommand = await callCLIPromise({
        commandPayload: {
            configure: null,
            '--set-farmer-peer': null,
            [`${$ip}:${$port}`]: null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog(`Successfully setup ip: ${$ip}, port: ${$port} for farmer peer`);
    } else {
        debugError(`Failed setup ip: ${$ip}, port: ${$port} for farmer peer`);
    }

    return checkCommand.status;
};
