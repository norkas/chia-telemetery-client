const HarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:add-plots-directory');
const state = require('../../state');

module.exports = async (directory) => {
    if (!directory) {
        debugError('No plots directory specified');
        return false;
    }

    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    const { success } = await HarvesterInstance.addPlotDirectory(directory);

    if (success) {
        debugLog(`Directory ${directory} with plots successfully added`);
    } else {
        debugError(`Failed to add directory ${directory} with plots`);
    }

    return success;
};
