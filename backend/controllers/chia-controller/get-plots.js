const getPlotMemo = require('../../utils/get-plot-memo');
const getPreparedPlotDataForServer = require('../../utils/get-prepared-plot-data-for-server');
const HarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:get-plots');
const state = require('../../state');

module.exports = async (forClient = false) => {
    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    let plotsData;

    try {
        plotsData = await HarvesterInstance.getPlots();
    } catch (error) {
        console.error(error);
        debugError('getPlots method by HarvesterInstance failed');
        return [];
    }

    if (!plotsData) {
        debugError('getPlots method by HarvesterInstance return undefined');
        return []
    }

    if (!plotsData.success) {
        debugError('getPlots method by HarvesterInstance success = false');
        return [];
    }

    const plots = plotsData.plots;

    if (!plots.length) {
        debugLog('User has no valid plots');
        return [];
    }

    const preparedPlots = [];

    for (const plot of plots) {
        preparedPlots.push({
            ...getPreparedPlotDataForServer(plot, forClient),
            memo: forClient ? undefined : await getPlotMemo(plot.filename)
        });
    }

    debugLog('Data on valid plots successfully collected');

    return preparedPlots;
};
